<?php 
namespace Survey\SurveyPage\Model\ResourceModel\Answer;

use \Magento\Framwork\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection 
{
    protected function __construct()
    {
        $this->__init('Survey\SurveyPage\Model\Answer'. 
                        'Survey\SurveyPage\Model\Resourcemodel\Answer');

    }
}