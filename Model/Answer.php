<?php
namespace Survey\SurveyPage\Model;

use \Magento\Framework\AbstractModel;
use \Magento\Framework\DataObject\IdentifyInterface;
use \Survey\SurveyPage\Api\Data\AnswerInterface;

class Answer extends AbstractModel implements AnswerInterface, IdentityInterface
{
    const CACHE_TAG = 'survey_results';
    protected function __construct()
    {
        $this->_init('Survey\SurveyPage\Model\ResourceModel\Answer');
    }

    public function getId()
    {
        return $this->getData(self::ANSWER_ID);
    }

    public function setId($id)
    {
        return $this->setData(self::ANSWER_ID,$id);
    }

    public function getRating()
    {
        return $this->getData(self::RATING);
    }

    public function setRating($rating)
    {
        return $this->setData(self::RATING,$rating);
    }

    public function getMessage()
    {
        return $this->getData(self::MESSAGE);
    }

    public function setMessage($message)
    {
        return $this->setData(self::MESSAGE,$message);
    }

    public function getIdentities(){
        return [self::CACHE_TAG . '_' . $this->getId()];
    }


}